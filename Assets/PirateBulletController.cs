﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PirateBulletController : MonoBehaviour {
    int cnt = 0;
    int fps = 60;
    public GameObject bullet;
    public Transform spawner;
	// Use this for initialization
	void Start () {
		
    }
    // Update is called once per frame
    void Update()
    {
        if (cnt++ > 1 * fps)
        {
            cnt = 0;
            Fire();
        }
    }

    void Fire()
    {
        var spawned = Instantiate(bullet, spawner.position, spawner.rotation);
        spawned.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 10;
        Destroy(spawned, 3.0f);
        Debug.Log("Dot shot");
    }
}

﻿using UnityEngine;

public class BattleShipController : MonoBehaviour {
    public GameObject bullet;
    public Transform spawner;


	// Use this for initialization
	void Start () {
		
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Fire();
        }
    }

    void Fire()
    {
        var spawned = Instantiate(bullet, spawner.position, spawner.rotation);
        spawned.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 10;
        Destroy(spawned, 3.0f);
        Debug.Log("Dot shot");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour {
    public GameObject bullet;
    public Transform spawner;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.A)) {
            Fire();
        }
	}

    void Fire(){
        var spawned = Instantiate(bullet, spawner);
        spawned.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;
        Destroy(spawned, 2.0f);
    }
}